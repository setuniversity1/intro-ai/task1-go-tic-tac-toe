package main

import (
	"fmt"
	"math"
)

const (
	BoardSize = 3
	EmptyCell = "-"
	PlayerX   = "X"
	PlayerO   = "O"
)

type GameState struct {
	Board       [BoardSize][BoardSize]string
	CurrentTurn string
}

func initializeGame() GameState {
	var game GameState
	for i := 0; i < BoardSize; i++ {
		for j := 0; j < BoardSize; j++ {
			game.Board[i][j] = EmptyCell
		}
	}
	game.CurrentTurn = PlayerX // Human player starts
	return game
}

func displayBoard(board [BoardSize][BoardSize]string) {
	for i := 0; i < BoardSize; i++ {
		for j := 0; j < BoardSize; j++ {
			fmt.Printf(" %s ", board[i][j])
			if j < BoardSize-1 {
				fmt.Print("|")
			}
		}
		fmt.Println()
		if i < BoardSize-1 {
			fmt.Println("---+---+---")
		}
	}
}

func isMoveLeft(board [BoardSize][BoardSize]string) bool {
	for i := 0; i < BoardSize; i++ {
		for j := 0; j < BoardSize; j++ {
			if board[i][j] == EmptyCell {
				return true
			}
		}
	}
	return false
}

func evaluate(board [BoardSize][BoardSize]string) int {
	winner := checkWinner(board)
	if winner == PlayerX {
		return -10 // If PlayerX wins, return a high negative value
	} else if winner == PlayerO {
		return 10 // If PlayerO wins, return a high positive value
	}

	xWins := checkWinningPatterns(board, PlayerX)
	oWins := checkWinningPatterns(board, PlayerO)

	return len(oWins) - len(xWins)
}

func checkWinningPatterns(board [BoardSize][BoardSize]string, player string) [][]int {
	var winningPatterns [][]int

	// Check rows for winning patterns
	for i := 0; i < BoardSize; i++ {
		if board[i][0] == player && board[i][1] == player && board[i][2] == player {
			winningPatterns = append(winningPatterns, []int{i, 0}, []int{i, 1}, []int{i, 2})
		}
	}

	// Check columns for winning patterns
	for j := 0; j < BoardSize; j++ {
		if board[0][j] == player && board[1][j] == player && board[2][j] == player {
			winningPatterns = append(winningPatterns, []int{0, j}, []int{1, j}, []int{2, j})
		}
	}

	// Check diagonals for winning patterns
	if board[0][0] == player && board[1][1] == player && board[2][2] == player {
		winningPatterns = append(winningPatterns, []int{0, 0}, []int{1, 1}, []int{2, 2})
	}
	if board[0][2] == player && board[1][1] == player && board[2][0] == player {
		winningPatterns = append(winningPatterns, []int{0, 2}, []int{1, 1}, []int{2, 0})
	}

	return winningPatterns
}

func minimax(board [BoardSize][BoardSize]string, depth int, isMax bool) int {
	score := evaluate(board)

	if score == 10 || score == -10 {
		return score
	}

	if !isMoveLeft(board) {
		return 0
	}

	if isMax {
		best := math.MinInt
		for i := 0; i < BoardSize; i++ {
			for j := 0; j < BoardSize; j++ {
				if board[i][j] == EmptyCell {
					board[i][j] = PlayerO
					best = int(math.Max(float64(best), float64(minimax(board, depth+1, !isMax))))
					board[i][j] = EmptyCell
				}
			}
		}
		return best
	} else {
		best := math.MaxInt
		for i := 0; i < BoardSize; i++ {
			for j := 0; j < BoardSize; j++ {
				if board[i][j] == EmptyCell {
					board[i][j] = PlayerX
					best = int(math.Min(float64(best), float64(minimax(board, depth+1, !isMax))))
					board[i][j] = EmptyCell
				}
			}
		}
		return best
	}
}

func findBestMove(board [BoardSize][BoardSize]string) (int, int) {
	bestVal := math.MinInt
	var bestMoveX, bestMoveY int

	for i := 0; i < BoardSize; i++ {
		for j := 0; j < BoardSize; j++ {
			if board[i][j] == EmptyCell {
				board[i][j] = PlayerO
				moveVal := minimax(board, 0, false)
				board[i][j] = EmptyCell

				if moveVal > bestVal {
					bestMoveX = i
					bestMoveY = j
					bestVal = moveVal
				}
			}
		}
	}
	return bestMoveX, bestMoveY
}

func main() {
	game := initializeGame()
	fmt.Println("Welcome to Tic Tac Toe!")
	displayBoard(game.Board)

	for {
		var row, col int

		if game.CurrentTurn == PlayerX {
			fmt.Println("Your turn (enter row and column, e.g., '0 0' for top-left):")
			fmt.Scan(&row, &col)

			if row < 0 || row >= BoardSize || col < 0 || col >= BoardSize || game.Board[row][col] != EmptyCell {
				fmt.Println("Invalid move. Try again.")
				continue
			}

			game.Board[row][col] = PlayerX
		} else {
			fmt.Println("Computer's turn:")
			row, col = findBestMove(game.Board)
			game.Board[row][col] = PlayerO
		}

		displayBoard(game.Board)

		winner := checkWinner(game.Board)
		if winner != EmptyCell {
			if winner == PlayerX {
				fmt.Println("Congratulations! You win!")
			} else {
				fmt.Println("Computer wins!")
			}
			break
		} else if !isMoveLeft(game.Board) {
			fmt.Println("It's a draw!")
			break
		}

		if game.CurrentTurn == PlayerX {
			game.CurrentTurn = PlayerO
		} else {
			game.CurrentTurn = PlayerX
		}
	}
}

func checkWinner(board [BoardSize][BoardSize]string) string {
	// Check rows
	for i := 0; i < BoardSize; i++ {
		if board[i][0] != EmptyCell && board[i][0] == board[i][1] && board[i][1] == board[i][2] {
			return board[i][0]
		}
	}

	// Check columns
	for i := 0; i < BoardSize; i++ {
		if board[0][i] != EmptyCell && board[0][i] == board[1][i] && board[1][i] == board[2][i] {
			return board[0][i]
		}
	}

	// Check diagonals
	if board[0][0] != EmptyCell && board[0][0] == board[1][1] && board[1][1] == board[2][2] {
		return board[0][0]
	}
	if board[0][2] != EmptyCell && board[0][2] == board[1][1] && board[1][1] == board[2][0] {
		return board[0][2]
	}

	return EmptyCell
}
